package services

import (
	"context"
	"math/rand"
	"time"
	"errors"
	"strconv"
	"sync"
)

type Wrk2APIService interface {
	ReadHomeTimeline(ctx context.Context, user_id int64, start int64, stop int64) ([]Post, error)
	ReadUserTimeline(ctx context.Context, user_id int64, start int64, stop int64) ([]Post, error)
	Follow(ctx context.Context, username string, followeeName string, user_id int64, followeeID int64) error
	Unfollow(ctx context.Context, username string, followeeName string, user_id int64, followeeID int64) error
	Register(ctx context.Context, firstName string, lastName string, username string, password string, user_id int64) error
	ComposePost(ctx context.Context, user_id int64, username string, post_type int64, text string, media_types []string, media_ids []int64) error
}

type Wrk2APIServiceImpl struct {
	userService UserService
	userTimelineService UserTimelineService
	homeTimelineService HomeTimelineService
	socialGraphService SocialGraphService
	textService TextService
	mediaService MediaService
	uniqueIDService UniqueIdService
}

func NewWrk2APIServiceImpl(userService UserService, userTimelineService UserTimelineService, homeTimelineService HomeTimelineService, socialGraphService SocialGraphService, textService TextService, mediaService MediaService, uniqueIDService UniqueIdService) *Wrk2APIServiceImpl {
	rand.Seed(time.Now().UnixNano())
	return &Wrk2APIServiceImpl{userService: userService, userTimelineService: userTimelineService, homeTimelineService: homeTimelineService, socialGraphService: socialGraphService, textService: textService, mediaService: mediaService, uniqueIDService: uniqueIDService}
}

func (w *Wrk2APIServiceImpl) ReadUserTimeline(ctx context.Context, user_id int64, start int64, stop int64) ([]Post, error) {
	reqID := rand.Int63()
	return w.userTimelineService.ReadUserTimeline(ctx, reqID, user_id, start, stop)
}

func (w *Wrk2APIServiceImpl) ReadHomeTimeline(ctx context.Context, user_id int64, start int64, stop int64) ([]Post, error) {
	reqID := rand.Int63()
	return w.homeTimelineService.ReadHomeTimeline(ctx, reqID, user_id, start, stop)
}

func (w *Wrk2APIServiceImpl) Follow(ctx context.Context, username string, followeeName string, user_id int64, followeeID int64) error {
	reqID := rand.Int63()
	if user_id != 0 && followeeID != 0 { 
		return w.socialGraphService.Follow(ctx, reqID, user_id, followeeID)
	} else if username != "" && followeeName != "" {
		return w.socialGraphService.FollowWithUsername(ctx, reqID, username, followeeName)
	}
	return errors.New("Invalid Arguments")
}

func (w *Wrk2APIServiceImpl) Unfollow(ctx context.Context, username string, followeeName string, user_id int64, followeeID int64) error {
	reqID := rand.Int63()
	if user_id != 0 && followeeID != 0 { 
		return w.socialGraphService.Unfollow(ctx, reqID, user_id, followeeID)
	} else if username != "" && followeeName != "" {
		return w.socialGraphService.UnfollowWithUsername(ctx, reqID, username, followeeName)
	}
	return errors.New("Invalid Arguments")
}

func (w *Wrk2APIServiceImpl) Register(ctx context.Context, firstName string, lastName string, username string, password string, user_id int64) error {
	if firstName == "" || lastName == "" || username == "" || password == "" {
		return errors.New("Incomplete Arguments")
	}
	reqID := rand.Int63()
	return w.userService.RegisterUserWithId(ctx, reqID, firstName, lastName, username, password, user_id)
}

func (w *Wrk2APIServiceImpl) ComposePost(ctx context.Context, user_id int64, username string, post_type string, text string, media_types []string, media_ids []int64) error {
	if user_id == 0 || username == "" || post_type == "" || text == "" {
		return errors.New("Incomplete Arguments")
	}
	reqID := rand.Int63()
	postInt, err := strconv.ParseInt(post_type, 10, 64)
	if err != nil {
		return err
	}
	postType := PostType(postInt)
	var wg sync.WaitGroup
	wg.Add(4)
	var err1, err2, err3, err4 error
	go func() {
		defer wg.Done()
		err1 = w.textService.UploadText(ctx, reqID, text)
	}()
	go func() {
		defer wg.Done()
		err2 = w.mediaService.UploadMedia(ctx, reqID, media_types, media_ids)
	}()
	go func() {
		defer wg.Done()
		err3 = w.uniqueIDService.UploadUniqueId(ctx, reqID, postType)
	}()
	go func() {
		defer wg.Done()
		err4 = w.userService.UploadCreatorWithUserId(ctx, reqID, user_id, username)
	}()
	wg.Wait()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	if err3 != nil {
		return err3
	}
	if err4 != nil {
		return err4
	}
	return nil
}
