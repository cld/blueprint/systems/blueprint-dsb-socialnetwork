package services

import (
	"context"
	"errors"
	"fmt"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"strconv"
)

type UserTimelineService interface {
	ReadUserTimeline(ctx context.Context, reqID int64, userID int64, start int64, stop int64) ([]Post, error)
	WriteUserTimeline(ctx context.Context, reqID int64, postID int64, userID int64, timestamp int64) error
}

type PostInfo struct {
	PostID int64
	Timestamp int64
}

type UserPosts struct {
	UserID int64
	Posts []PostInfo
}

type UserTimelineServiceImpl struct {
	userTimelineCache components.Cache
	userTimelineDB components.NoSQLDatabase
	postStorageService PostStorageService
}

func NewUserTimelineServiceImpl(userTimelineCache components.Cache, userTimelineDB components.NoSQLDatabase, postStorageService PostStorageService) *UserTimelineServiceImpl {
	return &UserTimelineServiceImpl{userTimelineCache: userTimelineCache, userTimelineDB: userTimelineDB, postStorageService: postStorageService}
}

func (u *UserTimelineServiceImpl) ReadUserTimeline(ctx context.Context, reqID int64, userID int64, start int64, stop int64) ([]Post, error) {
	if stop <= start || start < 0 {
		return []Post{}, nil
	}

	userIDStr := strconv.FormatInt(userID, 10)
	var post_infos []PostInfo
	u.userTimelineCache.Get(userIDStr, &post_infos)
	var post_ids []int64
	for _, post_info := range post_infos {
		post_ids = append(post_ids, post_info.PostID)
	}
	db_start := start + int64(len(post_ids))
	var new_post_ids []int64
	if db_start < stop {
		collection := u.userTimelineDB.GetDatabase("user-timeline").GetCollection("user-timeline")
		query := fmt.Sprintf(`{"UserID": %[1]d}`, userID)
		projection := fmt.Sprintf(`{"projection": {"posts": {"$slice": [0, %[1]d]}}}`, stop)
		post_db_val, err := collection.FindMany(query, projection)
		if err != nil {
			return []Post{}, err
		}
		post_db_val.All(&new_post_ids)
	}

	post_ids = append(post_ids, new_post_ids...)
	post_channel := make(chan bool)
	err_post_channel := make(chan error)
	var posts []Post
	go func() {
		var err error
		posts, err = u.postStorageService.ReadPosts(ctx, reqID, post_ids)
		if err != nil {
			err_post_channel <- err
			return
		}
		post_channel <- true
	}()

	if len(new_post_ids) > 0 {
		err := u.userTimelineCache.Put(userIDStr, post_ids)
		if err != nil {
			return []Post{}, err
		}
	}
	select {
	case <-post_channel:
		break
	case err := <-err_post_channel:
		return []Post{}, err
	}
	return posts, nil
}

func (u *UserTimelineServiceImpl) WriteUserTimeline(ctx context.Context, reqID int64, postID int64, userID int64, timestamp int64) error {
	collection := u.userTimelineDB.GetDatabase("user-timeline").GetCollection("user-timeline")

	query :=  fmt.Sprintf(`{"UserID": %[1]d}`, userID)
	results, err := collection.FindMany(query, "")
	var userPosts []UserPosts
	if err != nil {
		return err
	}
	results.All(&userPosts)

	if len(userPosts) == 0 {
		userPosts := UserPosts{UserID: userID, Posts: []PostInfo{PostInfo{PostID: postID, Timestamp: timestamp}}}
		err := collection.InsertOne(userPosts)
		if err != nil {
			return errors.New("Failed to insert user timeline user to Database")
		}
	} else {
		postIDstr := strconv.FormatInt(postID, 10)
		timestampstr := strconv.FormatInt(timestamp, 10)
		update := fmt.Sprintf(`{"$push": {"Posts": {"$each": [{"PostID": %s, "Timestamp": %s}], "$position": 0}}}`, postIDstr, timestampstr)
		err := collection.UpdateMany(query, update)
		if err != nil {
			return errors.New("Failed to insert user timeline user to Database")
		}
	}
	var postInfo []PostInfo
	userIDStr := strconv.FormatInt(userID, 10)
	// Ignore error check for Get!
	u.userTimelineCache.Get(userIDStr, &postInfo)
	postInfo = append(postInfo, PostInfo{PostID: postID, Timestamp: timestamp})
	return u.userTimelineCache.Put(userIDStr, postInfo)
}
