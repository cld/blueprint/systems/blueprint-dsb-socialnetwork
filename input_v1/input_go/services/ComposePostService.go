package services

import (
	"sync"
	"time"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib"
	"fmt"
)

type ComposePostService interface {
	UploadText(ctx context.Context, reqID int64, text string) error
	UploadCreator(ctx context.Context, reqID int64, creator Creator) error
	UploadUserMentions(ctx context.Context, reqID int64, user_mentions []UserMention) error
	UploadMedia(ctx context.Context, reqID int64, medias []Media) error
	UploadUniqueId(ctx context.Context, reqID int64, postID int64, postType PostType) error
	UploadUrls(ctx context.Context, reqID int64, urls []URL) error
}

type ComposePostServiceImpl struct {
	postStorageService PostStorageService
	userTimelineService UserTimelineService
	homeTimelineService HomeTimelineService
	composePostCache stdlib.Cache
	NUM_COMPONENTS int64
}

func NewComposePostServiceImpl(postStorageService PostStorageService, userTimelineService UserTimelineService, homeTimelineService HomeTimelineService, composePostCache stdlib.Cache) *ComposePostServiceImpl {
	return &ComposePostServiceImpl{postStorageService: postStorageService, userTimelineService: userTimelineService, homeTimelineService: homeTimelineService, composePostCache stdlib.Cache, NUM_COMPONENTS: 6}
}

func (c *ComposePostServiceImpl) UploadText(ctx context.Context, reqID int64, text string) error {
	req := fmt.Sprintf("%d",reqID)
	err := c.composePostCache.Put(req + ":text", text)
	if err != nil {
		return err
	}
	val, err := c.composePostCache.Incr(req + ":num_components")
	if err != nil {
		return err
	}
	if val == c.NUM_COMPONENTS {
		return c.composeAndUpload(ctx, req)
	}
	return nil
}

func (c *ComposePostServiceImpl) UploadCreator(ctx context.Context, reqID int64, creator Creator) error {
	req := fmt.Sprintf("%d",reqID)
	err := c.composePostCache.Put(req + ":creator", creator)
	if err != nil {
		return err
	}
	val, err := c.composePostCache.Incr(req + ":num_components")
	if err != nil {
		return err
	}
	if val == c.NUM_COMPONENTS {
		return c.composeAndUpload(ctx, req)
	}	
	return nil
}

func (c *ComposePostServiceImpl) UploadUserMentions(ctx context.Context, reqID int64, user_mentions []UserMention) error {
	req := fmt.Sprintf("%d",reqID)
	err := c.composePostCache.Put(req + ":user_mentions", creator)
	if err != nil {
		return err
	}
	val, err := c.composePostCache.Incr(req + ":num_components")
	if err != nil {
		return err
	}
	if val == c.NUM_COMPONENTS {
		return c.composeAndUpload(ctx, req)
	}	
	return nil
}

func (c *ComposePostServiceImpl) UploadMedia(ctx context.Context, reqID int64, medias []Media) error {
	req := fmt.Sprintf("%d",reqID)
	err := c.composePostCache.Put(req + ":media", medias)
	if err != nil {
		return err
	}
	val, err := c.composePostCache.Incr(req + ":num_components")
	if err != nil {
		return err
	}
	if val == c.NUM_COMPONENTS {
		return c.composeAndUpload(ctx, req)
	}	
	return nil
}

func (c *ComposePostServiceImpl) UploadUniqueId(ctx context.Context, reqID int64, postID int64, postType PostType) error {
	req := fmt.Sprintf("%d",reqID)
	postInfo := PostInfo{PostID: postID, Type: postType}
	err := c.composePostCache.Put(req + ":postinfo", postInfo)
	if err != nil {
		return err
	}
	val, err := c.composePostCache.Incr(req + ":num_components")
	if err != nil {
		return err
	}
	if val == c.NUM_COMPONENTS {
		return c.composeAndUpload(ctx, req)
	}	
	return nil
}

func (c *ComposePostServiceImpl) UploadUrls(ctx context.Context, reqID int64, urls []URL) error {
	req := fmt.Sprintf("%d",reqID)
	err := c.composePostCache.Put(req + ":urls", urls)
	if err != nil {
		return err
	}
	val, err := c.composePostCache.Incr(req + ":num_components")
	if err != nil {
		return err
	}
	if val == c.NUM_COMPONENTS {
		return c.composeAndUpload(ctx, req)
	}	
	return nil
}

func (c *ComposePostServiceImpl) composeAndUpload(ctx context.Context, reqID int64) error {
	timestamp := time.Now().UnixNano() / int64(time.Millisecond)
	req := fmt.Sprintf("%d", reqID)
	var err1, err2, err3, err4, err5, err6 error
	var uniqueID int64
	var creator Creator
	var up_text string
	var medias []Media
	var urls []URL
	var usermentions []UserMention
	var postInfo PostInfo
	var wg sync.WaitGroup
	wg.Add(6)
	go func() {
		defer wg.Done()
		err1 = c.composePostCache.Get(req + ":text", &up_text)
	}()
	go func() {
		defer wg.Done()
		err2 = c.composePostCache.Get(req + ":user_mentions", &usermentions)
	}()
	go func() {
		defer wg.Done()
		err3 = c.composePostCache.Get(req + ":media", &medias)
	}()
	go func() {
		defer wg.Done()
		err4 = c.composePostCache.Get(req + ":postinfo", &postInfo)
	}()
	go func() {
		defer wg.Done()
		err5 = c.composePostCache.Get(req + ":creator", &creator)
	}()
	go func() {
		defer wg.Done()
		err6 = c.composePostCache.Get(req + ":urls", &urls)
	}()
	wg.Wait()

	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	if err3 != nil {
		return err3
	}
	if err4 != nil {
		return err4
	}
	if err5 != nil {
		return err5
	}
	if err6 != nil {
		return err6
	}
	var post Post
	post.PostID = postInfo.PostID
	post.Creator = creator
	post.Medias = medias
	post.Text = up_text
	post.Urls = urls
	post.UserMentions = usermentions
	post.ReqID = reqID
	post.PostType = postInfo.PostType

	var usermentionIds []int64
	for _, um := range usermentions {
		usermentionIds = append(usermentionIds, um.UserID)
	}
	var wg2 sync.WaitGroup
	wg2.Add(3)
	go func() {
		defer wg2.Done()
		err1 = c.postStorageService.StorePost(ctx, reqID, post)
	}()
	go func() {
		defer wg2.Done()
		err2 = c.userTimelineService.WriteUserTimeline(ctx, reqID, uniqueID, userID, timestamp)
	}()
	go func() {
		defer wg2.Done()
		err3 = c.homeTimelineService.WriteHomeTimeline(ctx, reqID, uniqueID, userID, timestamp, usermentionIds)
	}()
	wg2.Wait()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return err3
}