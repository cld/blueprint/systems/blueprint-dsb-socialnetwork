package services

import (
	"context"
	"errors"
)

type MediaService interface {
	UploadMedia(ctx context.Context, reqID int64, mediaTypes []string, mediaIds []int64) error
}

type MediaServiceImpl struct {
	composePostService ComposePostService
}

func NewMediaServiceImpl(composePostService ComposePostService) *MediaServiceImpl {
	return &MediaServiceImpl{composePostService: composePostService}
}

func (m *MediaServiceImpl) UploadMedia(ctx context.Context, reqID int64, mediaTypes []string, mediaIds []int64) error {
	var medias []Media

	if len(mediaTypes) != len(mediaIds) {
		return medias, errors.New("The lengths of media_id list and media_type list are not equal")
	}

	for idx, mediaId := range mediaIds {
		media := Media{MediaID: mediaId, MediaType: mediaTypes[idx]}
		medias = append(medias, media)
	}

	return m.composePostService.UploadMedia(ctx, reqID, medias)
}