package services

import (
	"context"
	"math/rand"
	"time"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
)

type UrlShortenService interface {
	UploadUrls(ctx context.Context, reqID int64, urls []string) error
	GetExtendedUrls(ctx context.Context, reqID int64, shortened_urls []string) ([]string, error)
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

type UrlShortenServiceImpl struct {
	urlShortenDB components.NoSQLDatabase
	hostname string
	composePostService ComposePostService
}

func NewUrlShortenServiceImpl(urlShortenDB components.NoSQLDatabase, composePostService ComposePostService) *UrlShortenServiceImpl {
	rand.Seed(time.Now().UnixNano())
	return &UrlShortenServiceImpl{urlShortenDB: urlShortenDB, hostname: "http://short-url/", composePostService: composePostService}
}

func (u *UrlShortenServiceImpl) genRandomStr(length int) string {
	b := make([]rune, length)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func (u *UrlShortenServiceImpl) UploadUrls(ctx context.Context, reqID int64, urls []string) error {
	var target_urls []URL
	var target_url_docs []interface{}
	for _, url := range urls {
		shortened_url := u.hostname + u.genRandomStr(10)
		target_url := URL{ShortenedUrl: shortened_url, ExpandedUrl: url}
		target_urls = append(target_urls, target_url)
		target_url_docs = append(target_url_docs, target_url)
	}

	if len(target_urls) > 0 {
		collection := u.urlShortenDB.GetDatabase("url-shorten").GetCollection("url-shorten")
		err := collection.InsertMany(target_url_docs)
		if err != nil {
			return err
		}
	}

	return u.composePostService.UploadUrls(ctx, reqID, target_urls)
}

func (u *UrlShortenServiceImpl) GetExtendedUrls(ctx context.Context, reqID int64, shortened_urls []string) ([]string, error) {
	// Not implemented in Original DSB
	return []string{}, nil
}
