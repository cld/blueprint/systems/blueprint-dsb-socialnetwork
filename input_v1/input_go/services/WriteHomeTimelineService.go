package services

import (
	"encoding/json"
	"fmt"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"strconv"
	"context"
)

type WriteHomeTimelineService interface {
	Entry()
}

type WriteHomeTimelineServiceImpl struct {
	socialGraphService SocialGraphService
	homeTimelineCache components.Cache
	queue components.Queue
}

type Message struct {
	UserID int64
	ReqID int64
	PostID int64
	Timestamp int64
	UserMentionIDs []int64
}

func NewWriteHomeTimelineServiceImpl(socialGraphService SocialGraphService, homeTimelineCache components.Cache, queue components.Queue) *WriteHomeTimelineServiceImpl {
	return &WriteHomeTimelineServiceImpl{socialGraphService: socialGraphService, homeTimelineCache: homeTimelineCache, queue: queue}
}

func (w *WriteHomeTimelineServiceImpl) OnReceivedWorker(msg []byte) {
	var message Message
	json.Unmarshal(msg, &message)

	ctx := context.Background()
	followersID, err := w.socialGraphService.GetFollowers(ctx, message.ReqID, message.UserID)
	if err != nil {
		fmt.Println(err)
		return
	}
	followers := make(map[int64]bool)
	for _, f := range followersID {
		followers[f] = true
	}
	for _, id := range message.UserMentionIDs {
		followers[id] = true
	}
	for id := range followers {
		var postInfos []PostInfo
		w.homeTimelineCache.Get(strconv.FormatInt(id, 10), &postInfos)
		postInfos = append(postInfos, PostInfo{PostID: id, Timestamp: message.Timestamp})
		err = w.homeTimelineCache.Put(strconv.FormatInt(id, 10), postInfos)
		if err != nil {
			fmt.Println(err)
			return 
		}
	}
}

func (w *WriteHomeTimelineServiceImpl) Entry() {
	w.queue.Recv(w.OnReceivedWorker)
}