package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/debug"
	"strconv"
	"time"
)

type HomeTimelineService interface {
	ReadHomeTimeline(ctx context.Context, reqID int64, userID int64, start int64, stop int64) ([]Post, error)
}

type HomeTimelineServiceImpl struct {
	homeTimelineCache components.Cache
	postStorageService PostStorageService
}

func NewHomeTimelineServiceImpl(homeTimelineCache components.Cache, postStorageService PostStorageService) *HomeTimelineServiceImpl {
	return &HomeTimelineServiceImpl{homeTimelineCache: homeTimelineCache, postStorageService: postStorageService}
}

func (h *HomeTimelineServiceImpl) ReadHomeTimeline(ctx context.Context, reqID int64, userID int64, start int64, stop int64) ([]Post, error) {
	if stop <= start || start < 0 {
		return []Post{}, nil
	}
	userIDStr := strconv.FormatInt(userID, 10)
	var postIDs []int64
	var postInfos []PostInfo
	cache_start := time.Now()
	h.homeTimelineCache.Get(userIDStr, &postInfos)
	cache_dur := time.Now().Sub(cache_start)
	debug.ReportMetric("ReadHomeTimeline:CacheGet", cache_dur)
	for _, pinfo := range postInfos {
		postIDs = append(postIDs, pinfo.PostID)
	}
	if start < int64(len(postIDs)) {
		minstop := stop
		if stop > int64(len(postIDs)) {
			minstop = int64(len(postIDs))
		}
		postIDs = postIDs[start:minstop]
	}
	return h.postStorageService.ReadPosts(ctx, reqID, postIDs)
}
