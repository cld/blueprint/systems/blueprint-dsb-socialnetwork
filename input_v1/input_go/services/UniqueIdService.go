package services

import (
	"context"
	"strconv"
	"strings"
	"time"
	"net"
	"bytes"
)

type UniqueIdService interface {
	ComposeUniqueId(ctx context.Context, reqID int64, post_type PostType) error
}

type UniqueIdServiceImpl struct {
	counter int64
	current_timestamp int64
	machine_id string
	composePostService ComposePostService
}

// From: https://gist.github.com/tsilvers/085c5f39430ced605d970094edf167ba
func GetMachineID() string {
	interfaces, err := net.Interfaces()
    if err != nil {
        return "0"
    }

    for _, i := range interfaces {
        if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {

            // Skip locally administered addresses
            if i.HardwareAddr[0]&2 == 2 {
                continue
            }

            var mac uint64
            for j, b := range i.HardwareAddr {
                if j >= 8 {
                    break
                }
                mac <<= 8
                mac += uint64(b)
            }

            return strconv.FormatUint(mac, 16)
        }
    }

    return "0"
}

func NewUniqueIdServiceImpl(composePostService ComposePostService) *UniqueIdServiceImpl {
	return &UniqueIdServiceImpl{counter: 0, current_timestamp: -1, machine_id: GetMachineID(), composePostService: composePostService}
}

func (u *UniqueIdServiceImpl) getCounter(timestamp int64) int64 {
	if u.current_timestamp == timestamp {
		retVal := u.counter
		u.counter += 1
		return retVal
	} else {
		u.current_timestamp = timestamp
		u.counter = 1
		return 0
	}
}

func (u *UniqueIdServiceImpl) UploadUniqueId(ctx context.Context, reqID int64, post_type PostType) error {
	timestamp := time.Now().UnixNano() / int64(time.Millisecond)
	idx := u.getCounter(timestamp)
	timestamp_hex := strconv.FormatInt(timestamp, 16)
	if len(timestamp_hex) > 10 {
		timestamp_hex = timestamp_hex[:10]
	} else if len(timestamp_hex) < 10 {
		timestamp_hex = strings.Repeat("0", 10-len(timestamp_hex)) + timestamp_hex
	}
	counter_hex := strconv.FormatInt(idx, 16)
	if len(counter_hex) > 3 {
		counter_hex = counter_hex[:3]
	} else if len(counter_hex) < 3 {
		counter_hex = strings.Repeat("0", 3-len(counter_hex)) + counter_hex
	}
	post_id_str := u.machine_id + timestamp_hex + counter_hex
	post_id, err := strconv.ParseInt(post_id_str, 16, 64)
	if err != nil {
		return err
	}
	post_id = post_id & 0x7FFFFFFFFFFFFFFF
	return u.composePostService.UploadUniqueId(ctx, reqID, post_id)
}
