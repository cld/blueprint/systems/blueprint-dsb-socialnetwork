# blueprint-dsb-socialnetwork

Legacy Blueprint version of the Social Network application from the DeathStarBench microservice benchmark suite.

Visit https://github.com/Blueprint-uServices for the actively maintained version of Blueprint.
