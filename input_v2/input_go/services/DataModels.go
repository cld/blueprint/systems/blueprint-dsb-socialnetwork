package services

type User struct {
	UserID int64
	FirstName string
	LastName string
	Username string
	PwdHashed string
	Salt string
}

func (u User) remote() {}

type Media struct {
	MediaID int64
	MediaType string
}

func (m Media) remote() {}

type URL struct {
	ShortenedUrl string
	ExpandedUrl string
}

func (u URL) remote() {}

type UserMention struct {
	UserID int64
	Username string
}

func (u UserMention) remote() {}

type Creator struct {
	UserID int64
	Username string
}

func (c Creator) remote() {}

type PostType int

const (
	POST PostType = iota
	REPOST
	REPLY
	DM
)

type Post struct {
	PostID int64
	Creator Creator
	ReqID int64
	Text string
	UserMentions []UserMention
	Medias []Media
	Urls []URL
	Timestamp int64
	PostType PostType
}

func (p Post) remote() {}