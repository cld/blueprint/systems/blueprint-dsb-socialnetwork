package services

import (
	"context"
	"crypto/sha1"
    "encoding/base64"
	"errors"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"math/rand"
	"time"
	"github.com/dgrijalva/jwt-go"
	"strconv"
	"strings"
	"log"
)

type UserService interface {
	Login(ctx context.Context, reqID int64, username string, password string) (string, error)
	RegisterUserWithId(ctx context.Context, reqID int64, firstName string, lastName string, username string, password string, userID int64) error
	RegisterUser(ctx context.Context, reqID int64, firstName string, lastName string, username string, password string) error
	ComposeCreatorWithUserId(ctx context.Context, reqID int64, userID int64, username string) (Creator, error)
	ComposeCreatorWithUsername(ctx context.Context, reqID int64, username string) (Creator, error)
	GetUserId(ctx context.Context, reqID int64, username string) (int64, error)
}

type UserServiceImpl struct {
	machine_id string
	counter int64
	current_timestamp int64
	secret string
	userCache components.Cache
	userDB components.NoSQLDatabase
	socialGraphService SocialGraphService
}

type LoginObj struct {
	UserID int64
	Password string
	Salt string
}

type Claims struct {
	Username string
	UserID string
	Timestamp int64
	jwt.StandardClaims
}

func NewUserServiceImpl(userCache components.Cache, userDB components.NoSQLDatabase, socialGraphService SocialGraphService, secret string) *UserServiceImpl {
	return &UserServiceImpl{counter: 0, current_timestamp: -1,machine_id: GetMachineID(), userCache: userCache, userDB: userDB, socialGraphService: socialGraphService, secret: secret}
}

func (u *UserServiceImpl) getCounter(timestamp int64) int64 {
	if u.current_timestamp == timestamp {
		retVal := u.counter
		u.counter += 1
		return retVal
	} else {
		u.current_timestamp = timestamp
		u.counter = 1
		return 0
	}
}

func (u *UserServiceImpl) genRandomStr(length int) string {
	b := make([]rune, length)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func (u *UserServiceImpl) hashPwd(pwd []byte) string {
	hasher := sha1.New()
	hasher.Write(pwd)
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}

func (u *UserServiceImpl) Login(ctx context.Context, reqID int64, username string, password string) (string, error) {
	timestamp := time.Now().UnixNano() / int64(time.Millisecond)
	var login LoginObj
	login.UserID = -1
	err := u.userCache.Get(username + ":Login", &login)
	if err != nil {
		return "", err
	}
	if login.UserID == -1 {
		var user User
		collection := u.userDB.GetDatabase("user").GetCollection("user")
		query := `{"Username":"` + username + `"}`
		res, err := collection.FindOne(query, "")
		if err != nil {
			return "", err
		}
		res.Decode(&user)
		login.Password = user.PwdHashed
		login.Salt = user.Salt
		login.UserID = user.UserID
	}
	var tokenStr string
	hashed_pwd := u.hashPwd([]byte(password + login.Salt))
	if hashed_pwd != login.Password {
		return "", errors.New("Invalid credentials")
	} else {
		expiration_time := time.Now().Add(6 * time.Minute)
		claims := &Claims {
			Username: username,
			UserID: strconv.FormatInt(login.UserID, 10),
			Timestamp: timestamp,
			StandardClaims: jwt.StandardClaims{ ExpiresAt: expiration_time.Unix()},
		}
		var err error
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenStr, err = token.SignedString([]byte(u.secret))
		if err != nil {
			return "", errors.New("Failed to create login token")
		}
	}
	err = u.userCache.Put(username + ":Login", login)
	if err != nil {
		return "", err
	}
	return tokenStr, nil
}

func (u *UserServiceImpl) GetUserId(ctx context.Context, reqID int64, username string) (int64, error) {
	user_id := int64(-1)
	err := u.userCache.Get(username + ":UserID", &user_id)
	if err != nil {
		var user User
		collection := u.userDB.GetDatabase("user").GetCollection("user")
		query := `{"Username":"` + username + `"}`
		res, err := collection.FindOne(query, "")
		if err != nil {
			return -1, err
		}
		res.Decode(&user)
		user_id = user.UserID

		err = u.userCache.Put(username + ":UserID", user_id)
		if err != nil {
			return -1, err
		}
	}
	return user_id, nil
}

func (u *UserServiceImpl) ComposeCreatorWithUserId(ctx context.Context, reqID int64, userID int64, username string) (Creator, error) {
	return Creator{UserID: userID, Username: username}, nil
}

func (u *UserServiceImpl) ComposeCreatorWithUsername(ctx context.Context, reqID int64, username string) (Creator, error) {
	user_id := int64(-1)
	u.userCache.Get(username + ":UserID", &user_id)
	if user_id == -1 {
		var user User
		collection := u.userDB.GetDatabase("user").GetCollection("user")
		query := `{"Username":"` + username + `"}`
		res, err := collection.FindOne(query, "")
		if err != nil {
			return Creator{}, err
		}
		res.Decode(&user)
		user_id = user.UserID

		err = u.userCache.Put(username + ":UserID", user_id)
		if err != nil {
			return Creator{}, err
		}
	}
	return Creator{UserID: user_id, Username: username}, nil
}

func (u *UserServiceImpl) RegisterUserWithId(ctx context.Context, reqID int64, firstName string, lastName string, username string, password string, userID int64) error {
	log.Println("Registering UserID:", userID)
	collection := u.userDB.GetDatabase("user").GetCollection("user")
	query := `{"Username":"` +  username + `"}`
	res, err := collection.FindOne(query, "")
	if err != nil {
		return err
	}
	var user User
	user.UserID = -1
	res.Decode(&user)
	if user.UserID != -1 {
		return errors.New("Username already registered")
	}
	salt := u.genRandomStr(32)
	hashed_pwd := u.hashPwd([]byte(password + salt))
	user = User{UserID: userID, FirstName: firstName, LastName: lastName, PwdHashed: hashed_pwd, Salt: salt, Username: username}
	err = collection.InsertOne(user)
	if err != nil {
		return err
	}
	return u.socialGraphService.InsertUser(ctx, reqID, userID)
}

func (u *UserServiceImpl) RegisterUser(ctx context.Context, reqID int64, firstName string, lastName string, username string, password string) error {
	timestamp := time.Now().UnixNano() / int64(time.Millisecond)
	idx := u.getCounter(timestamp)
	timestamp_hex := strconv.FormatInt(timestamp, 16)
	if len(timestamp_hex) > 10 {
		timestamp_hex = timestamp_hex[:10]
	} else if len(timestamp_hex) < 10 {
		timestamp_hex = strings.Repeat("0", 10-len(timestamp_hex)) + timestamp_hex
	}
	counter_hex := strconv.FormatInt(idx, 16)
	if len(counter_hex) > 3 {
		counter_hex = counter_hex[:3]
	} else if len(counter_hex) < 3 {
		counter_hex = strings.Repeat("0", 3-len(counter_hex)) + counter_hex
	}
	user_id_str := u.machine_id + timestamp_hex + counter_hex
	user_id, err := strconv.ParseInt(user_id_str, 10, 64)
	if err != nil {
		return err
	}
	user_id = user_id & 0x7FFFFFFFFFFFFFFF
	return u.RegisterUserWithId(ctx, reqID, firstName, lastName, username, password, user_id)
}
