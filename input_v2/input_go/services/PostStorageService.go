package services

import (
	"context"
	"fmt"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/debug"
	"strconv"
	"sync"
	"errors"
	"strings"
	"log"
	"time"
)

type PostStorageService interface {
	StorePost(ctx context.Context, reqID int64, post Post) error
	ReadPost(ctx context.Context, reqID int64, postID int64) (Post, error)
	ReadPosts(ctx context.Context, reqID int64, postIDs []int64) ([]Post, error)
}

type PostStorageServiceImpl struct {
	postStorageCache components.Cache
	postStorageDB components.NoSQLDatabase
	CacheHits int64
	NumReqs int64
	CacheMiss int64
}

func startPostMetrics(p *PostStorageServiceImpl) {
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ticker.C:
			debug.ReportMetric("CacheMiss", float64(p.CacheMiss) / float64(p.NumReqs))
			p.NumReqs = 0
			p.CacheHits = 0
			p.CacheMiss = 0
		}
	}
}

func NewPostStorageServiceImpl(postStorageCache components.Cache, postStorageDB components.NoSQLDatabase) *PostStorageServiceImpl {
	p := &PostStorageServiceImpl{postStorageCache: postStorageCache, postStorageDB: postStorageDB}
	go startPostMetrics(p)
	return p
}

func (p *PostStorageServiceImpl) StorePost(ctx context.Context, reqID int64, post Post) error {
	collection := p.postStorageDB.GetDatabase("post").GetCollection("post")
	return collection.InsertOne(post)
}

func (p *PostStorageServiceImpl) ReadPost(ctx context.Context, reqID int64, postID int64) (Post, error) {
	var post Post
	err := p.postStorageCache.Get(strconv.FormatInt(postID, 10), &post)
	if err != nil {
		// Post was not in Cache, check DB!
		collection := p.postStorageDB.GetDatabase("post").GetCollection("post")
		query := fmt.Sprintf(`{"PostID": %[1]d`, postID)
		result, err := collection.FindOne(query, "")
		if err != nil {
			return post, err
		}
		err = result.Decode(&post)
		if err != nil {
			return post, errors.New("Post doesn't exist in MongoDB")
		}
	}
	return post, nil
}

func (p *PostStorageServiceImpl) ReadPosts(ctx context.Context, reqID int64, postIDs []int64) ([]Post, error) {
	unique_post_ids := make(map[int64]bool)
	for _, pid := range postIDs {
		unique_post_ids[pid] = true
	}
	//if len(unique_post_ids) != len(postIDs) {
	//	return []Post{}, errors.New("Post Ids are duplicated")
	//}
	var keys []string
	for _, pid := range postIDs {
		keys = append(keys, strconv.FormatInt(pid, 10))
	}
	values := make([]Post, len(keys))
	var retvals []interface{}
	for idx, _ := range values {
		retvals = append(retvals, &values[idx])
	}
	err := p.postStorageCache.Mget(keys, retvals)
	for _, post := range values {
		delete(unique_post_ids, post.PostID)
	}
	if err != nil {
		log.Println(err)
		//log.Println("Length of uniqueIDs", len(unique_post_ids), " ", len(postIDs))
	}
	p.NumReqs += 1
	if len(unique_post_ids) != 0 {
		p.CacheMiss += 1
		//log.Println("Current Cache Miss",p.CacheMiss)
		var new_posts []Post
		var unique_pids []int64
		for k := range unique_post_ids {
			unique_pids = append(unique_pids, k)
		}
		collection := p.postStorageDB.GetDatabase("post").GetCollection("post")
		delim := ","
		query := `{"PostID": {"$in": ` + strings.Join(strings.Fields(fmt.Sprint(unique_pids)), delim) + `}}`
		vals, err := collection.FindMany(query, "")
		if err != nil {
			log.Println(err)
			return []Post{}, err
		}
		vals.All(&new_posts)
		values = append(values, new_posts...)
		var wg sync.WaitGroup
		for _, new_post := range new_posts {
			wg.Add(1)

			go func() {
				defer wg.Done()
				p.postStorageCache.Put(strconv.FormatInt(new_post.PostID, 10), new_post)
			}()
		}
		wg.Wait()
	}
	return values, nil
}
