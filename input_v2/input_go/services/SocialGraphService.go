package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"strconv"
	"time"
	"sync"
)

type SocialGraphService interface {
	GetFollowers(ctx context.Context, reqID int64, userID int64) ([]int64, error)
	GetFollowees(ctx context.Context, reqID int64, userID int64) ([]int64, error)
	Follow(ctx context.Context, reqID int64, userID int64, followeeID int64) error
	Unfollow(ctx context.Context, reqID int64, userID int64, followeeID int64) error
	FollowWithUsername(ctx context.Context, reqID int64, userUsername string, followeeUsername string) error
	UnfollowWithUsername(ctx context.Context, reqID int64, userUsername string, followeeUsername string) error
	InsertUser(ctx context.Context, reqID int64, userID int64) error
}

type FollowerInfo struct {
	FollowerID int64
	Timestamp int64
}

type FolloweeInfo struct {
	FolloweeID int64
	Timestamp int64
}

type UserInfo struct {
	UserID int64
	Followers []FollowerInfo
	Followees []FolloweeInfo
}

type SocialGraphServiceImpl struct {
	socialGraphCache components.Cache
	socialGraphDB components.NoSQLDatabase
	userService UserService
}

func NewSocialGraphServiceImpl(socialGraphCache components.Cache, socialGraphDB components.NoSQLDatabase, userService UserService) *SocialGraphServiceImpl {
	return &SocialGraphServiceImpl{socialGraphCache: socialGraphCache, socialGraphDB: socialGraphDB, userService: userService}
}

func (s *SocialGraphServiceImpl) GetFollowers(ctx context.Context, reqID int64, userID int64) ([]int64, error) {
	var followers []int64
	var followerInfos []FollowerInfo
	userIDstr := strconv.FormatInt(userID, 10)
	err := s.socialGraphCache.Get(userIDstr + ":followers", &followerInfos)
	if err != nil {
		collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
		query := `{"UserID":` + userIDstr + `}`
		val, err := collection.FindOne(query, "")
		if err != nil {
			return followers, err
		}
		var userInfo UserInfo
		val.Decode(&userInfo)
		for _, follower := range userInfo.Followers {
			followers = append(followers, follower.FollowerID)
		}
		err = s.socialGraphCache.Put(userIDstr + ":followers", userInfo.Followers)
	} else {
		for _, f := range followerInfos {
			followers = append(followers, f.FollowerID)
		}
	}
	return followers, nil
}

func (s *SocialGraphServiceImpl) GetFollowees(ctx context.Context, reqID int64, userID int64) ([]int64, error) {
	var followees []int64
	var followeeInfos []FolloweeInfo
	userIDstr := strconv.FormatInt(userID, 10)
	err := s.socialGraphCache.Get(userIDstr + ":followees", &followeeInfos)
	if err != nil {
		collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
		query := `{"UserID":` + userIDstr + `}`
		val, err := collection.FindOne(query, "")
		if err != nil {
			return followees, err
		}
		var userInfo UserInfo
		val.Decode(&userInfo)
		for _, followee := range userInfo.Followees {
			followees = append(followees, followee.FolloweeID)
		}
		err = s.socialGraphCache.Put(userIDstr + ":followees", userInfo.Followees)
	} else {
		for _, f := range followeeInfos {
			followees = append(followees, f.FolloweeID)
		}
	}
	return followees, nil
}

func (s *SocialGraphServiceImpl) Follow(ctx context.Context, reqID int64, userID int64, followeeID int64) error {
	timestamp := time.Now()
	userIDstr := strconv.FormatInt(userID, 10)
	followeeIDstr := strconv.FormatInt(followeeID, 10)
	var err1, err2, err3 error
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
		query := `{"$and": [{"UserID":` + userIDstr + `}, {"followees": {"$not": {"$elemMatch": {"UserID": ` + followeeIDstr + `}}}}]}`
		update := `{"$push": {"followees": {"UserID": ` + userIDstr + `,"Timestamp": "` + timestamp.String() + `"}}}`
		err1 = collection.UpdateOne(query, update)
	}()
	go func() {
		defer wg.Done()
		collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
		query := `{"$and": [{"UserID":` + followeeIDstr + `}, {"followers": {"$not": {"$elemMatch": {"UserID": ` + followeeIDstr + `}}}}]}`
		update := `{"$push": {"followers": {"UserID": ` + followeeIDstr + `,"Timestamp": "` + timestamp.String() + `"}}}`
		err2 = collection.UpdateOne(query, update)
	}()
	go func() {
		defer wg.Done()
		var followees []FolloweeInfo
		var followers []FollowerInfo
		s.socialGraphCache.Get(userIDstr + ":followees", &followees)
		s.socialGraphCache.Get(followeeIDstr + ":followers", &followers)
		followees = append(followees, FolloweeInfo{FolloweeID: followeeID, Timestamp: timestamp.UnixNano()})
		followers = append(followers, FollowerInfo{FollowerID: userID, Timestamp: timestamp.UnixNano()})
		err3 = s.socialGraphCache.Put(userIDstr + ":followees", followees)
		if err3 != nil {
			return
		}
		err3 = s.socialGraphCache.Put(followeeIDstr + ":followers", followers)
	}()
	wg.Wait()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return err3
}

func (s *SocialGraphServiceImpl) Unfollow(ctx context.Context, reqID int64, userID int64, followeeID int64) error {
	userIDstr := strconv.FormatInt(userID, 10)
	followeeIDstr := strconv.FormatInt(followeeID, 10)
	var err1, err2, err3 error
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
		query := `{"UserID": ` + userIDstr + `}`
		update := `{"$pull": {"followees": {"UserID": `+ followeeIDstr+`}}}`
		err1 = collection.UpdateOne(query, update)
	}()
	go func() {
		defer wg.Done()
		collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
		query := `{"UserID": ` + followeeIDstr + `}`
		update := `{"$pull": {"followees": {"UserID": `+ userIDstr+`}}}`
		err2 = collection.UpdateOne(query, update)
	}()
	go func() {
		defer wg.Done()
		var followees []FolloweeInfo
		var followers []FollowerInfo
		var new_followers []FollowerInfo
		var new_followees []FolloweeInfo
		s.socialGraphCache.Get(userIDstr + ":followees", &followees)
		s.socialGraphCache.Get(followeeIDstr + ":followers", &followers)
		for _, followee := range followees { 
			if followee.FolloweeID != followeeID {
				new_followees = append(new_followees, followee)
			}
		}
		for _, follower := range followers {
			if follower.FollowerID != userID {
				new_followers = append(new_followers, follower)
			}
		}
		err3 = s.socialGraphCache.Put(userIDstr + ":followees", new_followees)
		if err3 != nil {
			return
		}
		err3 = s.socialGraphCache.Put(followeeIDstr + ":followers", new_followers)
	}()
	wg.Wait()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return err3
}

func (s *SocialGraphServiceImpl) FollowWithUsername(ctx context.Context, reqID int64, username string, followee_name string) error {
	var id int64
	var followee_id int64
	var err1 error
	var err2 error
	var wg sync.WaitGroup
	wg.Add(2)
	go func(){
		defer wg.Done()
		id, err1 = s.userService.GetUserId(ctx, reqID, username)
	}()
	go func() {
		defer wg.Done()
		followee_id, err2 = s.userService.GetUserId(ctx, reqID, followee_name)
	}()
	wg.Wait()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return s.Follow(ctx, reqID, id, followee_id)
}

func (s *SocialGraphServiceImpl) UnfollowWithUsername(ctx context.Context, reqID int64, username string, followee_name string) error {
	var id int64
	var followee_id int64
	var err1 error
	var err2 error
	var wg sync.WaitGroup
	wg.Add(2)
	go func(){
		defer wg.Done()
		id, err1 = s.userService.GetUserId(ctx, reqID, username)
	}()
	go func() {
		defer wg.Done()
		followee_id, err2 = s.userService.GetUserId(ctx, reqID, followee_name)
	}()
	wg.Wait()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return s.Unfollow(ctx, reqID, id, followee_id)
}

func (s *SocialGraphServiceImpl) InsertUser(ctx context.Context, reqID int64, userID int64) error {
	collection := s.socialGraphDB.GetDatabase("social-graph").GetCollection("social-graph")
	user := UserInfo{UserID: userID, Followers : []FollowerInfo{}, Followees: []FolloweeInfo{}}
	return collection.InsertOne(user)
}
