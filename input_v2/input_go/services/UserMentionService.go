package services

import (
	"context"
	"strings"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
	"log"
)

type UserMentionService interface {
	ComposeUserMentions(ctx context.Context, reqID int64, usernames []string) ([]UserMention, error)
}

type UserMentionServiceImpl struct {
	userCache components.Cache
	userDB components.NoSQLDatabase
}

func NewUserMentionServiceImpl(userCache components.Cache, userDB components.NoSQLDatabase) *UserMentionServiceImpl {
	return &UserMentionServiceImpl{userCache: userCache, userDB: userDB}
}

func (u *UserMentionServiceImpl) ComposeUserMentions(ctx context.Context, reqID int64, usernames []string) ([]UserMention, error) {
	usernames_not_cached := make(map[string]bool)
	rev_lookup := make(map[string]string)
	var keys []string
	for _, name := range usernames {
		usernames_not_cached[name] = true
		keys = append(keys, name + ":UserID")
		rev_lookup[name + ":UserID"] = name
	}
	values := make([]int64, len(keys))
	var retvals []interface{}
	for idx, _ := range values {
		retvals = append(retvals, &values[idx])
	}
	err := u.userCache.Mget(keys, retvals)
	if err != nil {
		return []UserMention{}, err
	}
	var user_mentions []UserMention
	for idx, key := range keys {
		user_mention := UserMention{UserID: values[idx], Username: rev_lookup[key]}
		user_mentions = append(user_mentions, user_mention)
		delete(usernames_not_cached, rev_lookup[key])
	}
	if len(usernames_not_cached) != 0 {
		var names []string
		for name := range usernames_not_cached {
			names = append(names, name)
		}
		collection := u.userDB.GetDatabase("user").GetCollection("user")
		query := `{"Username": {"$in": ` + strings.Join(strings.Fields(fmt.Sprint(names)), ",")+ `}}` 
		vals, err := collection.FindMany(query, "")
		if err != nil {
			return []UserMention{}, err
		}
		var new_user_mentions []UserMention
		vals.All(&new_user_mentions)
		user_mentions = append(user_mentions, new_user_mentions...)
	}
	log.Println(user_mentions)
	return user_mentions, nil
}
