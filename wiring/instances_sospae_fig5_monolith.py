default_client_opts : Callable[str, List[Modifier]] = lambda x : []
web_server : Modifier = WebServer(framework="default")
default_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})
normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})

jaegerTracer : Tracer = JaegerTracer().WithServer(normal_deployer)

jaegerTracerModifier: Callable[str, Modifier] = lambda x : TracerModifier(tracer=jaegerTracer, service_name=x)

web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [normal_deployer, web_server]
queue_modifiers : Callable[str, List[Modifier]] = lambda x : [default_deployer]

socialGraphDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
postStorageDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
userTimelineDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
urlShortenDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
userDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
mediaDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)

postStorageCache : Cache = Memcached().WithServer(default_deployer)
urlShortenCache : Cache = Memcached().WithServer(default_deployer)
userCache : Cache = Memcached().WithServer(default_deployer)
mediaCache : Cache = Memcached().WithServer(default_deployer)
socialGraphCache : Cache = RedisCache().WithServer(default_deployer)
homeTimelineCache : Cache = RedisCache().WithServer(default_deployer)
composePostCache : Cache = RedisCache().WithServer(default_deployer)
userTimelineCache : Cache = Memcached().WithServer(default_deployer)

postStorageService : Service = PostStorageServiceImpl(postStorageCache=postStorageCache, postStorageDatabase=postStorageDatabase)

urlShortenService : Service = UrlShortenServiceImpl(urlShortenDatabase=urlShortenDatabase)

userMentionService : Service = UserMentionServiceImpl(userCache=userCache, userDatabase=userDatabase)

textService : Service = TextServiceImpl(urlShortenService=urlShortenService, userMentionService=userMentionService)

mediaService : Service = MediaServiceImpl()

userTimelineService : Service = UserTimelineServiceImpl(userTimelineCache=userTimelineCache, userTimelineDatabase=userTimelineDatabase, postStorageService=postStorageService)

userIDService : Service = UserServiceImpl(userCache=userCache, userDatabase=userDatabase, socialGraphService=socialGraphService, secret="secret")

socialGraphService : Service = SocialGraphServiceImpl(socialGraphCache=socialGraphCache, socialGraphDatabase=socialGraphDatabase, userService=userIDService)

homeTimelineService : Service = HomeTimelineServiceImpl(homeTimelineCache=homeTimelineCache, postStorageService=postStorageService, socialGraphService=socialGraphService)

userService : Service = UserServiceImpl(userCache=userCache, userDatabase=userDatabase, socialGraphService=socialGraphService,secret="secret")

uniqueIdService : Service = UniqueIdServiceImpl()

composePostService : Service = ComposePostServiceImpl(postStorageService=postStorageService,userTimelineService=userTimelineService, userService=userService, uniqueIdService=uniqueIdService, mediaService=mediaService, textService=textService, homeTimelineService=homeTimelineService)


wrk2apiService : Service = Wrk2APIServiceImpl(userService=userService,composePostService=composePostService, userTimelineService=userTimelineService, homeTimelineService=homeTimelineService, socialGraphService=socialGraphService).WithServer(web_server_modifiers("wrk2apiService"))

monolith : Process = Process(services=[postStorageService, urlShortenService, userMentionService, textService, mediaService, userTimelineService, userIDService, socialGraphService, homeTimelineService, userService, uniqueIdService, composePostService, wrk2apiService])
