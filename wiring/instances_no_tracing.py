default_client_opts : Modifier = ClientPool(max_clients=128)
rpc_server : Modifier = RPCServer(framework="aiothrift")
web_server : Modifier = WebServer(framework="default")
default_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})
normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})

server_modifiers : Callable[str, List[Modifier]] = lambda x : [rpc_server, default_deployer]
client_modifiers : List[Modifier] = [default_client_opts]
web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [normal_deployer, web_server]
queue_modifiers : Callable[str, List[Modifier]] = lambda x : [default_deployer]

socialGraphDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
postStorageDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
userTimelineDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
urlShortenDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
userDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
mediaDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)

postStorageCache : Cache = Memcached().WithServer(default_deployer)
urlShortenCache : Cache = Memcached().WithServer(default_deployer)
userCache : Cache = Memcached().WithServer(default_deployer)
mediaCache : Cache = Memcached().WithServer(default_deployer)
socialGraphCache : Cache = RedisCache().WithServer(default_deployer)
homeTimelineCache : Cache = RedisCache().WithServer(default_deployer)
composePostCache : Cache = RedisCache().WithServer(default_deployer)
userTimelineCache : Cache = RedisCache().WithServer(default_deployer)

writeHomeTimelineQueue : Queue = RabbitMQ(queue_name="default_queue").WithServer(queue_modifiers("writeHomeTimelineQueue"))

postStorageService : Service = PostStorageServiceImpl(postStorageCache=postStorageCache, postStorageDatabase=postStorageDatabase).WithServer(server_modifiers("postStorageService")).WithClient(default_client_opts)

urlShortenService : Service = UrlShortenServiceImpl(urlShortenDatabase=urlShortenDatabase).WithServer(server_modifiers("urlShortenService")).WithClient(default_client_opts)

userMentionService : Service = UserMentionServiceImpl(userCache=userCache, userDatabase=userDatabase).WithServer(server_modifiers("userMentionService")).WithClient(default_client_opts)

textService : Service = TextServiceImpl(urlShortenService=urlShortenService, userMentionService=userMentionService).WithServer(server_modifiers("textService")).WithClient(default_client_opts)

mediaService : Service = MediaServiceImpl().WithServer(server_modifiers("mediaService")).WithClient(default_client_opts)

userTimelineService : Service = UserTimelineServiceImpl(userTimelineCache=userTimelineCache, userTimelineDatabase=userTimelineDatabase, postStorageService=postStorageService).WithServer(server_modifiers("userTimelineService")).WithClient(default_client_opts)

homeTimelineService : Service = HomeTimelineServiceImpl(homeTimelineCache=homeTimelineCache, postStorageService=postStorageService).WithServer(server_modifiers("homeTimelineService")).WithClient(default_client_opts)

userService : Service = UserServiceImpl(userCache=userCache, userDatabase=userDatabase, socialGraphService=socialGraphService).WithServer(server_modifiers("UserService")).WithClient(default_client_opts)

socialGraphService : Service = SocialGraphServiceImpl(socialGraphDatabase=socialGraphDatabase, socialGraphCache=socialGraphCache, userService=userService).WithServer(server_modifiers("socialGraphService")).WithClient(default_client_opts)

uniqueIdService : Service = UniqueIdServiceImpl().WithServer(server_modifiers("uniqueIdService")).WithClient(default_client_opts)

composePostService : Service = ComposePostServiceImpl(userTimelineService=userTimelineService, postStorageService=postStorageService, mediaService=mediaService, textService=textService, homeTimelineService=homeTimelineService, userService=userService, uniqueIdService=uniqueIdService).WithServer(server_modifiers("composePostService")).WithClient(default_client_opts)


wrk2apiService : Service = Wrk2APIServiceImpl(homeTimelineService=homeTimelineService, userTimelineService=userTimelineService, userService=userService, socialGraphService=socialGraphService, composePostService=composePostService).WithServer(web_server_modifiers("wrk2apiService"))