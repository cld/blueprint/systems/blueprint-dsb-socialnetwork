default_client_opts : Callable[str,Modifier] = lambda x : ClientPool(max_clients=512, metrics=True, service_name=x)
rpc_server : Modifier = RPCServer(framework="aiothrift", metrics=True)
web_server : Modifier = WebServer(framework="default", metrics=True)
default_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})
normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})

jaegerTracer : Tracer = JaegerTracer().WithServer(normal_deployer)

jaegerTracerModifier: Callable[str, Modifier] = lambda x : TracerModifier(tracer=jaegerTracer, service_name=x)

server_modifiers : Callable[str, List[Modifier]] = lambda x : [rpc_server, default_deployer]
client_modifiers : List[Modifier] = [default_client_opts]
web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [normal_deployer, web_server]
queue_modifiers : Callable[str, List[Modifier]] = lambda x : [default_deployer]

socialGraphDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
postStorageDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
userTimelineDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
urlShortenDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
userDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)
mediaDatabase : NoSQLDatabase = MongoDB().WithServer(default_deployer)

postStorageCache : Cache = Memcached().WithServer(default_deployer)
urlShortenCache : Cache = Memcached().WithServer(default_deployer)
userCache : Cache = Memcached().WithServer(default_deployer)
mediaCache : Cache = Memcached().WithServer(default_deployer)
socialGraphCache : Cache = RedisCache().WithServer(default_deployer)
homeTimelineCache : Cache = RedisCache().WithServer(default_deployer)
composePostCache : Cache = RedisCache().WithServer(default_deployer)
userTimelineCache : Cache = RedisCache().WithServer(default_deployer)

writeHomeTimelineQueue : Queue = RabbitMQ(queue_name="default_queue").WithServer(queue_modifiers("writeHomeTimelineQueue"))

postStorageService : Service = PostStorageServiceImpl(postStorageCache=postStorageCache, postStorageDatabase=postStorageDatabase).WithServer(server_modifiers("postStorageService")).WithClient(default_client_opts("postStorage"))

urlShortenService : Service = UrlShortenServiceImpl(urlShortenDatabase=urlShortenDatabase).WithServer(server_modifiers("urlShortenService")).WithClient(default_client_opts("urlShorten"))

userMentionService : Service = UserMentionServiceImpl(userCache=userCache, userDatabase=userDatabase).WithServer(server_modifiers("userMentionService")).WithClient(default_client_opts("userMention"))

textService : Service = TextServiceImpl(urlShortenService=urlShortenService, userMentionService=userMentionService, composePostService=composePostService).WithServer(server_modifiers("textService")).WithClient(default_client_opts("text"))

mediaService : Service = MediaServiceImpl(composePostService=composePostService).WithServer(server_modifiers("mediaService")).WithClient(default_client_opts("media"))

userTimelineService : Service = UserTimelineServiceImpl(userTimelineCache=userTimelineCache, userTimelineDatabase=userTimelineDatabase, postStorageService=postStorageService).WithServer(server_modifiers("userTimelineService")).WithClient(default_client_opts("userTimeline"))

homeTimelineService : Service = HomeTimelineServiceImpl(homeTimelineCache=homeTimelineCache, postStorageService=postStorageService, socialGraphService=socialGraphService).WithServer(server_modifiers("homeTimelineService")).WithClient(default_client_opts("homeTimeline"))

userService : Service = UserServiceImpl(userCache=userCache, userDatabase=userDatabase, socialGraphService=socialGraphService,composePostService=composePostService, secret="secret").WithServer(server_modifiers("UserService")).WithClient(default_client_opts("user"))

socialGraphService : Service = SocialGraphServiceImpl(socialGraphCache=socialGraphCache, socialGraphDatabase=socialGraphDatabase, userService=userService).WithServer(server_modifiers("socialGraphService")).WithClient(default_client_opts("socialGraph"))

uniqueIdService : Service = UniqueIdServiceImpl().WithServer(server_modifiers("uniqueIdService")).WithClient(default_client_opts("uniqueId"))

composePostService : Service = ComposePostServiceImpl(postStorageService=postStorageService,userTimelineService=userTimelineService, homeTimelineService=homeTimelineService, composePostCache=composePostCache).WithServer(server_modifiers("composePostService")).WithClient(default_client_opts("composePost"))


wrk2apiService : Service = Wrk2APIServiceImpl(userService=userService, userTimelineService=userTimelineService, homeTimelineService=homeTimelineService, socialGraphService=socialGraphService, textService=textService, mediaService=mediaService, uniqueIdService=uniqueIdService).WithServer(web_server_modifiers("wrk2apiService"))
